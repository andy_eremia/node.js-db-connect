//import users from '../apis/index';
import axios from 'axios'
import history from '../history';


export const adaugaCurs = (curs) => {
    console.log(curs);
    return(dispatch) => {
        return axios.post('http://localhost:8080/cursuri', curs).then(response => {
            console.log(response);
            history.push('/cursuri');
        }).catch(err => console.log(err));
    }
};


const _stergeCurs = ({idcursuri} = {}) => ({
    type: 'STERGE_CURS',
    idcursuri
});

export const stergeCurs = (idcursuri) => {
    console.log(idcursuri);
    
    return(dispatch) => {
        return axios.delete(`http://localhost:8080/cursuri/${idcursuri}`).then(() => {
            dispatch(_stergeCurs({idcursuri}));
            //console.log({idcursuri});
            history.push('/cursuri');
        })
    }
};

const _editeazaCurs = (idcursuri, updates) => ({
    type: 'EDIT_CURS',
    idcursuri,
    updates
});

export const editeazaCurs = (idcursuri, updates) => {
    return (dispatch) => {
        return axios.put(`http://localhost:8080/cursuri/${idcursuri}`, updates).then(() => {
            dispatch(_editeazaCurs(idcursuri, updates));
            history.push('/cursuri');
        });
    }
};

const _cautaCursuri = (cursuri) => ({
    type: 'FETCH_CURSURI',
    cursuri
});

export const cautaCursuri = () => {
    return (dispatch) => {
        return axios.get('http://localhost:8080/cursuri').then(result => {
            const cursuri = [];

            result.data.forEach(item => {
                cursuri.push(item);
            });
            dispatch(_cautaCursuri(cursuri));
            //history.push('/cursuri');
        });
    }
}

const _cautaCurs = (curs) => ({
    type: 'FETCH_CURS',
    curs
});

export const cautaCurs = (idcursuri) => {
    return (dispatch) => {
        //console.log(dispatch);
        return axios.get(`http://localhost:8080/cursuri/${idcursuri}`).then(result => {
            const curs = result.data;
            console.log(curs);
            dispatch(_cautaCurs(curs));
            //history.push('/cursuri');
        });
    }
}



const _getUsers = (users) => ({
    type: 'FETCH_USERS',
    users
});

export const fetchUsers = () =>{

    return(dispatch) => {
        return axios.get('http://localhost:8080/users').then(result => {
            const users = [];

            result.data.forEach(item => {
                users.push(item);
            });
            dispatch(_getUsers(users));
        });
    };
};

export const login = (user) => {
    //console.log(user);
    return(dispatch) => {
        return axios.post('http://localhost:8080/login', user).then(response => {
            localStorage.setItem("jwtToken", response.data.access_token);
            const time = new Date();
            localStorage.setItem("start", time.getHours()*3600 + time.getMinutes()*60 + time.getSeconds());
            localStorage.setItem("exp", response.data.expiresIn);
            //console.log(localStorage.getItem('start'));
            if(response.data.access_token){
                axios.defaults.headers.common["Authorization"] ="Bearer " + response.data.access_token;
            } else delete axios.defaults.header.common["Authorization"];
            console.log(response);
            history.push('/cursuri');
        }).catch(err => console.log(err));
    }
};

export const register = (user) => {
    console.log(user);
    return(dispatch) => {
        return axios.post('http://localhost:8080/register', user).then(response => {
            console.log(response);
            history.push('/');
        }).catch(err => console.log(err));
    }
};


import React from 'react'
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {login} from '../actions/index.js';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';

class LoginPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email : '',
      password: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);

  
  }
  
  handleInputChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }
  onSubmit = (event) => {
    //console.log(this.state);
    event.preventDefault();
    this.props.login(this.state);
    alert('Authentication coming soon!');

  }
  render() {
    //console.log(this.state);
    return (
      <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
        <Grid.Column style={{ maxWidth: 450 }}>
          <Header as='h2' color='teal' textAlign='center'>
            Login below!
          </Header>
            <Form size='large' onSubmit={this.onSubmit}>
              <Segment stacked>
                <Form.Input
                  icon='user'
                  fluid
                  type="email"
                  name="email"
                  placeholder="Enter email"
                  value={this.state.email}
                  onChange={this.handleInputChange}
                  required
                />
                <Form.Input
                  fluid
                  icon='lock'
                  type="password"
                  name="password"
                  placeholder="Enter password"
                  value={this.state.password}
                  onChange={this.handleInputChange}
                  required
                />
                <Button color='teal' fluid size='large' type="submit" >Login</Button>
              <Message>New to us? <Link to='/register'>Register</Link></Message>
            </Segment>
          </Form>
        </Grid.Column>
      </Grid>
    );
    
  }
}

const mapStateToProps = (state) => {
  return {user: state.user};
}

export default connect(mapStateToProps, {login})(LoginPage);
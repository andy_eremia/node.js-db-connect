import React from 'react';
import {connect} from 'react-redux';
import {register} from '../actions/index.js';
import { Button, Form, Grid, Header, Segment } from 'semantic-ui-react';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          nume: '',
          email: '',
          password: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
      }
      handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({
          [name]: value
        });
      }
      onSubmit = (event) => {
        event.preventDefault();
        this.props.register(this.state);
        alert('Registration coming soon!');
      }
      render() {
        return (
          <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
              <Header as='h2' color='teal' textAlign='center'>
                Register below!
              </Header>
              <Form size='large' onSubmit={this.onSubmit}>
                <Segment stacked>
                  <Form.Input
                    icon='user'
                    fluid
                    type="name"
                    name="nume"
                    placeholder="Enter your name"
                    value={this.state.nume}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    icon='user'
                    fluid
                    type="email"
                    name="email"
                    placeholder="Enter email"
                    value={this.state.email}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    icon='lock'
                    type="password"
                    name="password"
                    placeholder="Enter password"
                    value={this.state.password}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Button color='teal' fluid size='large' type="submit" >Register</Button>
                </Segment>
              </Form>
            </Grid.Column>
          </Grid>
        );
      }
}

const mapStateToProps = (state) => {
  return {usr: state.usr};
}

export default connect(mapStateToProps, {register})(RegisterPage);
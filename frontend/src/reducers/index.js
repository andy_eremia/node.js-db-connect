import {combineReducers} from 'redux';
import usersReducer from './usersReducer';
import loginReducer from './loginReducer';
import registerReducer from './registerReducer';
import cursuriReducer from './cursuriReducer';

export default combineReducers({
    users: usersReducer,
    user: loginReducer,
    usr: registerReducer,
    cursuri: cursuriReducer
});
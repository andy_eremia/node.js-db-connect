const initialState = {
    usr: []
};

export default(state = initialState, action) => {
    switch(action.type){
        case 'REGISTER':
            return {
                ...state, 
                usr: action.usr
            };
        default:
            return state; 
    }
}
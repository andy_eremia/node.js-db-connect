//import _ from 'lodash';
const cursuriReducerDefaultState = [];

export default(state = cursuriReducerDefaultState, action) => {
    switch(action.type){
        case 'ADD_CURS':
            return [
                ...state,
                action.curs
            ];
        case 'EDIT_CURS':
            console.log(state);
            
            return action.updates;
        case 'STERGE_CURS':
            return state;
        case 'FETCH_CURSURI':
            return action.cursuri;
        case 'FETCH_CURS':
            return action.curs;
        default:
            return state;
    }
};
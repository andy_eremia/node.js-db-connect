import React from 'react';
import {connect} from 'react-redux';
import {fetchUsers} from '../actions/index.js';


class UserList extends React.Component {


    componentDidMount() {
        this.props.fetchUsers();
    }

    renderList(){
        //console.log(this.props.users.users);
        return this.props.users.users.map(user => {
            return (

                <div className="item" key={user.idusers}>
                    <div className="content">
                        <p>{user.nume}</p>
                        <p>{user.email}</p>
                    </div>
                </div>
            );
        });
    }

    render() {
    return <div className="ui relaxed divided list">{this.renderList()}</div>;
    }
}

const mapStateToProps = (state) => {
    return {users: state.users};
}

export default connect(mapStateToProps, {fetchUsers})(UserList);
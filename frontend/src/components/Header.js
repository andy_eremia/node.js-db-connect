import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <div className="ui secondary pointing menu">
      <Link to="/home" className="item">
        FULLSTACK
      </Link>
      <div className="right menu">
      <button>
          <Link to="/adaugacurs" className="item">
            Adauga curs
          </Link>
        </button>
        <button onClick = {
          () => {
            localStorage.clear();
          }
        }>
          <Link to="/" className="item">
            Logout
          </Link>
        </button>
        
      </div>
    </div>
  );
};

export default Header;
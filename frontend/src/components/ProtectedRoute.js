import React from 'react';
import {Redirect, Route} from 'react-router-dom';

export const ProtectedRoute = ({component: Component, ...rest}) => (
    <Route
    {...rest}
    
    render={props => 
        
        parseInt(localStorage.getItem('start') + localStorage.getItem('exp')) >= (new Date().getHours()*3600 + new Date().getMinutes()*60 + new Date().getSeconds()) ? (
            <Component {...props} />
        ) : (
            <Redirect to = {{
                pathname: "/",
                state: {from: props.location}
            }}
            />
        )
    }
    />
);

export default ProtectedRoute;
import React from 'react';
//import axios from 'axios';
import {Router, Route, Switch} from 'react-router-dom';

import LoginPage from '../ui_pages/LoginPage.js';
import RegisterPage from '../ui_pages/RegisterPage.js';
import UserList from './UserList';
import ProtectedRoute from './ProtectedRoute';
import Header from './Header';
import history from '../history';
import Home from './Home.js';
import AdaugaCurs from './cursuri/AdaugaCurs';
import CautaCursuri from './cursuri/CautaCursuri';
import StergeCurs from './cursuri/StergeCurs';
import EditeazaCurs from './cursuri/EditeazaCurs';

const App = () => {
  //console.log(localStorage.getItem('jwtToken'));
  // if(localStorage.getItem('jwtToken')){
  //   axios.defaults.headers.common["Authorization"] ="Bearer " + localStorage.getItem('jwtToken');
  // } else delete axios.defaults.header.common["Authorization"];
    return (
      
      <div>
        <Router history={history}>
          <Switch>
            <div>
              <Header />
              <Route path="/" exact component = {LoginPage}>
              </Route>
              <Route path="/register" exact component = {RegisterPage}/>
              <ProtectedRoute path="/users" exact component = {UserList}/>
              <ProtectedRoute exact path="/home" component = {Home}/>
              <ProtectedRoute exact path="/adaugacurs" component = {AdaugaCurs}/>
              <ProtectedRoute exact path="/cursuri" component = {CautaCursuri}/>
              <ProtectedRoute exact path="/stergecurs/:id" component = {StergeCurs}/>
              <ProtectedRoute exact path="/editeazacurs/:id" component = {EditeazaCurs}/>
            </div>
          </Switch>
        </Router>
      </div>
    );
  };
  
  export default App;
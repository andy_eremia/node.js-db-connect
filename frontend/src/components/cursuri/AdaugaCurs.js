import React from 'react';
import {connect} from 'react-redux';
import {adaugaCurs} from '../../actions/index';
import { Button, Form, Grid, Header, Segment } from 'semantic-ui-react';

class AdaugaCurs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          Denumire: '',
          Sala: ''
        };

        this.handleInputChange = this.handleInputChange.bind(this);
      }
      handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({
          [name]: value
        });
      }
      onSubmit = (event) => {
        event.preventDefault();
        this.props.adaugaCurs(this.state);
        
      }
      render() {
        return (
          <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
              <Header as='h2' color='teal' textAlign='center'>
                Adaugati un curs
              </Header>
              <Form size='large' onSubmit={this.onSubmit}>
                <Segment stacked>
                  <Form.Input
                    fluid
                    type="text"
                    name="Denumire"
                    placeholder="Denumire curs"
                    value={this.state.Denumire}
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    type="text"
                    name="Sala"
                    placeholder="Sala curs"
                    value={this.state.Sala}
                    onChange={this.handleInputChange}
                    required
                  />
                  
                  <Button color='teal' fluid size='large' type="submit" >Adaugare</Button>
                </Segment>
              </Form>
            </Grid.Column>
          </Grid>
        );
      }
}

const mapStateToProps = (state) => {
  return {cursuri: state.cursuri};
}

export default connect(mapStateToProps, {adaugaCurs})(AdaugaCurs);
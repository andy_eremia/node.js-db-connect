import React from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {cautaCursuri} from '../../actions/index';
//import { Button } from 'semantic-ui-react';

import {stergeCurs} from '../../actions/index';

class CautaCursuri extends React.Component {

    componentDidMount() {
        
        this.props.cautaCursuri();
        
        //console.log(this.props);
    }

    renderList(){
        console.log(this.props.cursuri);
        console.log(this.props);
        if(this.props.cursuri.length > 1)
        {return this.props.cursuri.map(curs => {
            return (

                <div className="item" key={curs.idcursuri}>
                    <div className="content">
                        <p>Denumire: {curs.Denumire}</p>
                        <p>Sala: {curs.Sala}</p>
                    </div>
                    <div className="right floated content">
                        <Link to={`/editeazacurs/${curs.idcursuri}`} className="ui button primary">
                            Edit
                        </Link>
                        <Link
                            to={`/stergecurs/${curs.idcursuri}`}
                            className="ui button negative"
                        >
                            Delete
                        </Link>
                    </div>
                </div>
            );
        });}
    }

    render() {
        return <div className="ui relaxed divided list">{this.renderList()}</div>;
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {cursuri: state.cursuri};
}

export default connect(mapStateToProps, {cautaCursuri, stergeCurs})(CautaCursuri);
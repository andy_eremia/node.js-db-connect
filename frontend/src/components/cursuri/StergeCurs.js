import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
//import history from '../../history';
import {cautaCurs, stergeCurs} from '../../actions/index';
//import Modal from 'react-modal-bootstrap';
//import Modal from 'react-bootstrap/Modal';


class StergeCurs extends React.Component {
    
    componentDidMount(){
        console.log(this.props);
        this.props.cautaCurs(this.props.match.params.id);
        
    }

    renderActions() {
        
        return(
            <React.Fragment>
                <button
                    onClick={() => this.props.stergeCurs(this.props.curs.idcursuri)}
                    className="ui button negative"
                >
                    Delete
                </button>
                <Link to="/cursuri" className="ui button">
                    Cancel
                </Link>
            </React.Fragment>
        );
    }

    renderContent() {
        if (!this.props.curs) {
          return `Esti sigur ca vrei sa stergi cursul:`;
        }
    
        return `Esti sigur ca vrei sa stergi cursul: ${
          this.props.curs.Denumire
        }`;
      }

    render() {
        //console.log(this.props.cursuri);
        return (

            <div>
                {this.renderContent()}
                {this.renderActions()}
            </div>
            
        );
        
    }
}

const mapStateToProps = (state) => {
    console.log(state);
    return {curs: state.cursuri};
    
    
}

export default connect(mapStateToProps, {cautaCurs, stergeCurs})(StergeCurs);
import React from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {editeazaCurs, cautaCurs} from '../../actions/index';
import { Button, Form, Grid, Header, Segment } from 'semantic-ui-react';


class EditeazaCurs extends React.Component {

    componentDidMount(){
        console.log(this.props);
        this.props.cautaCurs(this.props.match.params.id);
    }

    handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({
          [name]: value
        });
      }
      onSubmit = (event) => {
        event.preventDefault();
        this.props.editeazaCurs(this.props.curs.idcursuri, this.state);
        
      }

    render() {
        console.log(this.props.curs);
        console.log(this.props);
        //return <div>a</div>
        //if(this.props.curs<=1){
        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
              <Header as='h2' color='teal' textAlign='center'>
                Adaugati un curs
              </Header>
              <Form size='large' onSubmit={this.onSubmit}>
                <Segment stacked>
                  <Form.Input
                    fluid
                    type="text"
                    name="Denumire"
                    placeholder={this.props.curs.Denumire}
                    
                    onChange={this.handleInputChange}
                    required
                  />
                  <Form.Input
                    fluid
                    type="text"
                    name="Sala"
                    placeholder={this.props.curs.Sala}
                    
                    onChange={this.handleInputChange}
                    required
                  />
                  
                  <Button color='teal' fluid size='large' type="submit" >Editare</Button>
                  <Link to="/cursuri" className="ui button">
                    Cancel
                </Link>
                </Segment>
              </Form>
            </Grid.Column>
          </Grid>
        );
    //}
    }


}


const mapStateToProps = (state) => {
    console.log(state);
    return {curs: state.cursuri};
    
}

export default connect(mapStateToProps, {cautaCurs, editeazaCurs})(EditeazaCurs);
//Aplicatia are la baza un server web creat cu framework-ul Express
//si este realizata dupa design patternul MVC, workflow-ul fiind urmatorul:
//
//-> clientul trimite catre server cereri de tip HTTP pe care aplicatia le gestioneaza cu ajutorul
//fisierului profesor.routes.js din directorul routes
//-> fisierul profesor.routes.js interprezeaza cererile HTTP si directioneaza comenzile catre 
//controller-ul aflat in directorul controllers
//-> controller-ul interpreteaza inputul venit de la user si il trimite catre procesare modelului
//aflat in fisierul models/profesor.model.js
//-> modelul lucreaza cu inputul propriu-zis, manipuland tabela profesori din baza de date dbconnect
//


const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var cors = require("cors");
app.use(cors());
app.use(function (err, req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header(
    "Access-Control-Allow-Methods",
    "GET,HEAD,OPTIONS,POST,PUT,DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
});

app.get("/", (req, res) => {
    res.json({message: "Aplicatia lui Andy Eremia"});
});

require("./app/routes/profesor.routes.js")(app);
require("./app/routes/curs.routes.js")(app);
require("./app/routes/user.routes.js")(app);

app.listen(8080, () => {
    console.log("Server running on port 8080");
});
// creare obiect
const person = {
    isHuman: false,
    printIntroduction: function() {
      console.log(`My name is ${this.name}. Am I human? ${this.isHuman}`);
    }
  };
  
  const me = Object.create(person);
  
  me.name = 'Matthew'; // "name" is a property set on "me", but not on "person"
  me.isHuman = true; // inherited properties can be overwritten
  
 // me.printIntroduction();
  // expected output: "My name is Matthew. Am I human? true"

//definire proprietati obiecte
const object1 = {};

Object.defineProperty(object1, 'property1', {
  value: 42
  //writable: false
});

object1.property1 = 77;

//console.log(object1.property1);

//object freeze
const obj = {
    prop: 42
  };
  
  Object.freeze(obj);
  
  obj.prop = 33;
  
  //console.log(obj.prop);


//object assign (copiaza toate proprietatile unui obiect -> e un fel de object copy)
const target = { a: 1, b: 2 };
const source = { b: 4, c: 5 };

const returnedTarget = Object.assign(target, source);

//console.log(returnedTarget);


//Object.entries() -> returneaza un array format din proprietatile obiectului
const object5 = {
    a: 'somestring',
    b: 42
  };
  
  for (const [key, value] of Object.entries(object5)) {
    //console.log(`${key}: ${value}`);
  }


//Object.keys() -> returneaza numele cheilor proprietatilor obiectelor in ordine
const object3 = {
    a: 'somestring',
    b: 42,
    c: false
  };
  
  //console.log(Object.keys(object3));


//Object.values() -> returneaza valorile proprietatilor unui obiect
const object2 = {
    a: 'somestring',
    b: 42,
    c: false
  };
  
  console.log(Object.values(object2));
// creare array-uri
let shoppingList = ['cereale', 'branza', 'rosii'];

let lotto = [45,25,49,33,1,5];

let mix = new Array(12, 'caine', true, null, NaN);

//console.log(mix);
//console.log(mix.length);;

//accesare prin index

let culori = ['verde', 'rosu', 'albastru', 'galben'];
//console.log(culori[3]);

//modificare array-uri: spre deosebire de striguri care sunt imutabile, arrayurile sunt mutabile
shoppingList[1] = 'inghetata';
//console.log(shoppingList);
shoppingList[shoppingList.length] = 'mere'; // daca nu stim lungimea arrayului, se adauga la final
//console.log(shoppingList);

//push & pop & shift & unshift
let nume = ['dan', 'ilinca'];
nume.push('andrei');
nume.push('andra');
const a = nume.pop();
//console.log(nume);

nume.unshift('petrica');
nume.unshift('george');
nume.unshift('miruna');
//console.log(nume);
//console.log(nume.shift());
//console.log(nume);


//concatenare
let fructe = ['mar', 'banana'];
let legume = ['rosii', 'ardei'];
let carnuri = ['porc', 'pui'];
//console.log(fructe.concat(legume));
//console.log(legume.concat(fructe));
//console.log(legume.concat(fructe,carnuri));

//includes si indexOf
let ingrediente = ['apa', 'ulei','faina','vanilie','oua','zahar','unt'];
//console.log(ingrediente.includes('zahar'));
//console.log(ingrediente.includes('vanilie', 4));
//console.log(ingrediente.indexOf('faina'));

//reverse & join
let litere = ['a', 'b', 'c', 'd', 'e'];
//console.log(litere.reverse());
//console.log(litere.join('`'));


//slice
let animale = ['rechin', 'urs', 'vultur','leu','bizon', 'pisica'];

let animale1 = animale.slice(0,2);
//console.log(animale1);
let animale2 = animale.slice(-4, -2);
//console.log(animale2);

//splice - removes/replaces/adds elements in array
animale.splice(1,0,'peste');
//console.log(animale);
animale.splice(2,1);
//console.log(animale);


//sort
let fete = ['Ana', 'Miruna','Bianca','Carmen','Andreea','Raluca'];
fete.sort();
//console.log(fete);

let nr = [10, 3, 10000, 25];
//console.log(nr.sort()); //sorteaza dupa strig
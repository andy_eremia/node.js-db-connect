//interpretarea comenzilor din controller
//se manevreaza informatia propriu-zisa cu ajutorul bazei de date

const sql = require("./db.js");

//constructor Profesor pt a manipula mai usor datele prin CRUD
const Profesor = function(profesor) {
    this.Nume = profesor.Nume;
    this.Prenume = profesor.Prenume;
    this.Adresa = profesor.Adresa;
    this.idcursuri = profesor.idcursuri;
};

//adaugarea unui nou prof in db
Profesor.create = (newProfesor, result) => {
    sql.query(`INSERT INTO dbconnect.profesori SET ?`, newProfesor, (err, res) => {
        if(err){
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("Profesor creat: ", {id: res.insertId, ...newProfesor});
        result(null, {id: res.insertId, ...newProfesor});
        
    });
};


//cauta prof dupa id
Profesor.findById = (profesorId, result) => {
    sql.query(`SELECT * FROM dbconnect.profesori WHERE idprofesori = ${profesorId}`, (err, res) => {
        if(err) {
            console.log(("error: ", err));
            result(err, null);
            return;
        }
        if(res.length) {
            console.log("Am gasit profesorul: ", res[0]);
            result(null, res[0]);
            return;
        }
        result({kind: "not_found"}, null);
    });
};

//se cauta toti profesorii care au id.cursuri comun cu cel specificat in ruta
//din cererea HTTP  GET:/profesori/cursuri/:cursId
//vor fi afisati profesorii cu Nume, Prenume, Adresa
Profesor.findByForeignId = (cursId, result) => {
  sql.query(`SELECT profesori.Nume, profesori.Prenume, profesori.Adresa 
            FROM profesori, cursuri
            WHERE profesori.idcursuri = cursuri.idcursuri AND cursuri.idcursuri = ${cursId}`, (err, res) => {
              if(err) {
                console.log(("error: ", err));
                result(err, null);
                return;
            }
            
              if(res.length) {
                console.log("Am gasit profesorii: ", res);
                result(null, res);
                return;
            }
              result({kind: "not_found"}, null);
            });
};


//afiseaza toata tabela profesori
Profesor.getAll = result => {
    sql.query(`SELECT * FROM dbconnect.profesori`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("profesori: ", res);
      result(null, res);
    });
  };

  //update prof dupa id
  Profesor.updateById = (id, profesor, result) => {
    sql.query(
      `UPDATE dbconnect.profesori SET Nume = ?, Prenume = ?, Adresa = ?, idcursuri = ? WHERE idprofesori = ?`,
      [profesor.Nume, profesor.Prenume, profesor.Adresa, profesor.idcursuri, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("Informatii actualizate ", { id: id, ...profesor });
        result(null, { id: id, ...profesor });
      }
    );
  };

  //sterge dupa id
  Profesor.remove = (id, result) => {
    sql.query(`DELETE FROM dbconnect.profesori WHERE idprofesori = ?`, id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("Am sters profesorul cu id ", id);
      result(null, res);
    });
  };

  //sterge toata tabela
  Profesor.removeAll = result => {
    sql.query(`DELETE FROM dbconnect.profesori`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Am sters ${res.affectedRows} profesori");
      result(null, res);
    });
  };
  
  module.exports = Profesor;
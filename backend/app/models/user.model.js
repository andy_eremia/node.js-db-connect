// am dezvoltat functiile de creare user si cautare user dupa email
//ele vor folosi atat in functionalitatea de register/create user verificand daca emailul compeltat de utilizator
//este existent, dar si in functionalitatea de login pt a verifica corectitudinea campurilor completate

const sql = require("./db.js");

const User = function(user) {
    this.nume = user.nume;
    this.email = user.email;
    this.parola = user.parola;
};

User.create = (newUser, result) => {
    
    sql.query(`INSERT INTO dbconnect.users SET ?`, newUser, (err, res) => {
        if(err){
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("User creat: ", {id: res.insertId, ...newUser});
        result(null, {id: res.insertId, ...newUser});
        
    });
};


User.findByEmail = (email, result) => {
    sql.query(`SELECT * FROM dbconnect.users WHERE email = '${email}'`, (err, res) => {
        if(err) {
            console.log(("error: ", err));
            result(err, null);
            return;
        }
        if(res.length) {
            console.log("Am gasit utilizatorul: ", res[0]);
            result(null, res[0]);
            return;
        }
        result({kind: "not_found"}, null);
    });
};


User.getAll = result => {
    sql.query(`SELECT * FROM dbconnect.users`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("users: ", res);
      result(null, res);
    });
  };

module.exports = User;
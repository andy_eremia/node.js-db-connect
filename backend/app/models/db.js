const mysql = require("mysql");
const dbconfig = require("../config/db.config.js");


//creare conexiune la db

const con = mysql.createConnection({
    host: dbconfig.HOST,
    user: dbconfig.USER,
    password: dbconfig.PASSWORD,
    database: dbconfig.DB
});

con.connect(err => {
    if(err) throw err;
    console.log("Conexiune reusita!");
    
});

module.exports = con;
const sql = require("./db.js");

const Curs = function(curs) {
    this.Denumire = curs.Denumire;
    this.Sala = curs.Sala;
};

Curs.create = (newCurs, result) => {
    //console.log(newCurs.create);
    sql.query(`INSERT INTO dbconnect.cursuri SET ?`, newCurs, (err, res) => {
        if(err){
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("Profesor creat: ", {id: res.insertId, ...newCurs});
        result(null, {id: res.insertId, ...newCurs});
        
    });
};



Curs.findById = (cursId, result) => {
    sql.query(`SELECT * FROM dbconnect.cursuri WHERE idcursuri = ${cursId}`, (err, res) => {
        if(err) {
            console.log(("error: ", err));
            result(err, null);
            return;
        }
        if(res.length) {
            console.log("Am gasit cursul: ", res[0]);
            result(null, res[0]);
            return;
        }
        result({kind: "not_found"}, null);
    });
};


Curs.getAll = result => {
    sql.query(`SELECT * FROM dbconnect.cursuri`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("cursuri: ", res);
      result(null, res);
    });
  };

  Curs.updateById = (id, curs, result) => {
    sql.query(
      `UPDATE dbconnect.cursuri SET Denumire = ?, Sala = ? WHERE idcursuri = ?`,
      [curs.Denumire, curs.Sala, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("Informatii actualizate ", { id: id, ...curs });
        result(null, { id: id, ...curs });
      }
    );
  };

  Curs.remove = (id, result) => {
    sql.query(`DELETE FROM dbconnect.cursuri WHERE idcursuri = ?`, id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("Am sters cursul cu id ", id);
      result(null, res);
    });
  };

  Curs.removeAll = result => {
    sql.query(`DELETE FROM dbconnect.cursuri`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("Am sters ${res.affectedRows} cursuri");
      result(null, res);
    });
  };
  
  module.exports = Curs;
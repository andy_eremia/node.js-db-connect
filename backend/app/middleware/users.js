const User = require("../models/user.model.js");
const jwt = require('jsonwebtoken');    // creare json web token
const bcrypt = require('bcryptjs');     //modul criptare parola

const SECRET_KEY = "secret123";


exports.authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, SECRET_KEY, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};
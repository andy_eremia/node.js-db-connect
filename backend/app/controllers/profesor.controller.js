//in acest fisier se gestioneaza inputul venit de la user
//trimitand comenzi catre model

const Profesor = require("../models/profesor.model.js");


exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
      }
    
      // Creaza prof
      const profesor = new Profesor({
        Nume: req.body.Nume,
        Prenume: req.body.Prenume,
        Adresa: req.body.Adresa,
        idcursuri: req.body.idcursuri
      });
    
      // Salveaza prof in db
      Profesor.create(profesor, (err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Eroare de creare prof."
          });
        else res.send(data);
      });
};

exports.findAll = (req, res) => {
    Profesor.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Eroare in cautare profi."
          });
        else res.send(data);
      });
};

exports.findOne = (req, res) => {
    Profesor.findById(req.params.profesorId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Nu s-a gasit prof cu id ${req.params.profesorId}.`
            });
          } else {
            res.status(500).send({
              message: "Eroare la prof cu id " + req.params.profesorId
            });
          }
        } else res.send(data);
      });
};

exports.findOneByForeignId = (req, res) => {
  
  Profesor.findByForeignId(req.params.cursId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Nu s-au gasit profesori pt acest curs cu id ${req.params.cursId}.`
        });
      } else {
        res.status(500).send({
          message: "Eroare la profesori la cursul cu id " + req.params.cursId
        });
      }
    } 
    else res.send(data);
  });
};

exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
      }
    
      Profesor.updateById(
        req.params.profesorId,
        new Profesor(req.body),
        (err, data) => {
          if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Nu s-a gasit prof cu id ${req.params.profesorId}.`
              });
            } else {
              res.status(500).send({
                message: "Eroare la actualizare dupa id " + req.params.profesorId
              });
            }
          } else res.send(data);
        }
      );
};

exports.delete = (req, res) => {
    Profesor.remove(req.params.profesorId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Nu s-a gasit prof cu id ${req.params.profesorId}.`
            });
          } else {
            res.status(500).send({
              message: "Nu se poate sterge prof cu id " + req.params.profesorId
            });
          }
        } else res.send({ message: `Prof sters!` });
      });
};

exports.deleteAll = (req, res) => {
    Profesor.removeAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Eroare"
          });
        else res.send({ message: "Toti profii au fost stersi din db" });
      });
};
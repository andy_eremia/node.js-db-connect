
const Curs = require("../models/curs.model.js");


exports.create = (req, res) => {
    console.log(req.body);
    if (!req.body) {
        
        res.status(400).send({
          message: "Content can not be empty!"
        });
      }
    
      // Creaza prof
      const curs = new Curs({
        Denumire: req.body.Denumire,
        Sala: req.body.Sala
      });
    
      // Salveaza prof in db
      Curs.create(curs, (err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Eroare de creare curs."
          });
        else res.send(data);
      });
};

exports.findAll = (req, res) => {
    Curs.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Eroare in cautare cursuri."
          });
        else res.send(data);
      });
};

exports.findOne = (req, res) => {
    Curs.findById(req.params.cursId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Nu s-a gasit curs cu id ${req.params.cursId}.`
            });
          } else {
            res.status(500).send({
              message: "Eroare la curs cu id " + req.params.cursId
            });
          }
        } else res.send(data);
      });
};

exports.update = (req, res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
      }
    
      Curs.updateById(
        req.params.cursId,
        new Curs(req.body),
        (err, data) => {
          if (err) {
            if (err.kind === "not_found") {
              res.status(404).send({
                message: `Nu s-a gasit curs cu id ${req.params.cursId}.`
              });
            } else {
              res.status(500).send({
                message: "Eroare la actualizare dupa id " + req.params.cursId
              });
            }
          } else res.send(data);
        }
      );
};

exports.delete = (req, res) => {
  console.log(req.params);
    Curs.remove(req.params.cursId, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Nu s-a gasit curs cu id ${req.params.cursId}.`
            });
          } else {
            res.status(500).send({
              message: "Nu se poate sterge curs cu id " + req.params.cursId
            });
          }
        } else res.send({ message: `Curs sters!` });
      });
};

exports.deleteAll = (req, res) => {
    Curs.removeAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Eroare"
          });
        else res.send({ message: "Toate cursurile au fost sterse din db" });
      });
};
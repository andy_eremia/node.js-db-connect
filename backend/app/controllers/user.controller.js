//

const User = require("../models/user.model.js");
const jwt = require('jsonwebtoken');    // creare json web token
const bcrypt = require('bcryptjs');     //modul criptare parola

const SECRET_KEY = "secret123";

// const refreshTokenSecret = 'secretRefresh';
// const refreshTokens = [];

//creare user, adaugare in baza de date alaturi de parola criptata
exports.createUser = (req, res) => {
  console.log(req.body);
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
      }
      const user = new User({
        nume: req.body.nume,
        email: req.body.email,
        parola: bcrypt.hashSync(req.body.password)
      });
    console.log(req.body);
      User.create(user, (err, data) => {
        if (err)
            res.status(500).send({
            message:
              err.message || "Eroare de creare user."
          });
          else {
          User.findByEmail(req.body.email, (err, user) => {
              if(err) return  res.status(500).send('Server error!');
              const expiresIn = 24*60*60;
              const accesToken = jwt.sign({email: user.email}, SECRET_KEY, {expiresIn: expiresIn});
              res.status(200).send({data, "access_token": accesToken, "expiresIn": expiresIn});
          });
        }
      });
};


//login user, verificare credentiale
exports.login = (req, res) => {
  console.log(req.body);
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
    }
    
    const email= req.body.email;
    const parola= req.body.password;
    User.findByEmail(email, (err, user) => {
        if(err) return res.status(500).send("Eroare server!");
        if(!user) return res.status(404).send("Nu exista utilizator pt datele introduse!");
        const result = bcrypt.compareSync(parola, user.parola);
        if(!result) return res.status(401).send("Parola incorecta");
        const expiresIn = 60*60*24;
        const accessToken = jwt.sign({email: user.email}, SECRET_KEY, {expiresIn: expiresIn});
        // //creaza si token pt
        // const refreshToken = jwt.sign({ email: user.email}, refreshTokenSecret);
        // refreshTokens.push(refreshToken);
        res.status(200).send({"user": user, "access_token": accessToken, "expiresIn": expiresIn});
    });
}

exports.findAll = (req, res) => {
  User.getAll((err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Eroare in cautare useri."
        });
      else res.send(data);
    });
};
module.exports = app => {
    const cursuri = require("../controllers/curs.controller.js");
    const auth = require("../middleware/users");
    
    app.post("/cursuri",auth.authenticateJWT, cursuri.create);
    app.get("/cursuri",auth.authenticateJWT, cursuri.findAll);
    app.get("/cursuri/:cursId", cursuri.findOne);
    app.put("/cursuri/:cursId",auth.authenticateJWT, cursuri.update);
    app.delete("/cursuri/:cursId",auth.authenticateJWT, cursuri.delete);
    app.delete("/cursuri", cursuri.deleteAll);
  };
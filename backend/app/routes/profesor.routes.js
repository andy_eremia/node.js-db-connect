module.exports = app => {
    const profesori = require("../controllers/profesor.controller.js");
  
    //raspunsul serverului la cererile HTTP primite de la client
    //pt SELECT, UPDATE, DELETE dupa id /profesori/:profesorid
    //pt INSERT si SELECT, DELETE ALL /profesori
    
    app.post("/profesori", profesori.create);
    app.get("/profesori", profesori.findAll);
    app.get("/profesori/:profesorId", profesori.findOne);
    //ruta pentru JOIN dintre profesori si cursuri
    //se vor cauta profesorii care au acelasi idcursuri
    app.get("/profesori/cursuri/:cursId", profesori.findOneByForeignId);
    //
    app.put("/profesori/:profesorId", profesori.update);
    app.delete("/profesori/:profesorId", profesori.delete);
    app.delete("/profesori", profesori.deleteAll);
  };
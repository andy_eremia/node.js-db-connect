//rutele de register si login pt user
//
// Workflow inregistrare:
//-> se creeaza un nou user cu datele nume, email, parola (criptata)
//-> se verifica existenta emailului introdus
//-> email existent -> eroare
//-> email inexistent -> creare user + generare jwt
//
//Workflow login:
//-> se verfica existenta in baza de date a credentialelor introduse
//-> daca acestea sunt corecte, se realizeaza login si se genereaza jwt valabila 24h

module.exports = app => {

    const users = require("../controllers/user.controller.js");
    const auth = require("../middleware/users");

    app.post("/register", users.createUser);
    app.post("/login", users.login);
    app.get("/users",auth.authenticateJWT, users.findAll);
    //app.get("/users", users.findAll);
    app.post('/logout', (req, res) => {
        res.send("Logout successful");
    });
}
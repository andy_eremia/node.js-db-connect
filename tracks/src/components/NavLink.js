import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import Spacer from './Spacer';
import { useNavigation, useLinkProps } from '@react-navigation/native';

const NavLink = ({ text, routeName, onClear }) => {
    const navigation = useNavigation();
    return (
        <TouchableOpacity onPress={() => { navigation.navigate(routeName); onClear()}}>
            <Spacer>
                <Text style={styles.linkStyle}>{text}</Text>
            </Spacer>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    linkStyle: {
        color: 'blue'
    }
});

export default NavLink;
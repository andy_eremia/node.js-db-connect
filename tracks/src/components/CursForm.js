import React, { useState } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button, Input } from 'react-native-elements';
import Spacer from './Spacer';

const CursForm = ({ onSubmit, initialValues }) => {
    
    const [Denumire, setDenumire] = useState(initialValues.Denumire);
    const [Sala, setSala] = useState(initialValues.Sala);

    return (
        <View>
            <Text style={styles.labelStyle}>Denumire curs: </Text>
            <Spacer />
            <Input 
                style={styles.inputStyle}
                value={Denumire}
                onChangeText={(text) => setDenumire(text)}
            />
            <Spacer />
            <Text style={styles.labelStyle}>Sala curs: </Text>
            <Spacer />
            <Input 
                style={styles.inputStyle}
                value={Sala}
                onChangeText={(text) => setSala(text)}
            />
            <Spacer />
            <Button 
                title="Inregistreaza curs"
                onPress={() => {
                    onSubmit(Denumire, Sala)
                }}
            />
        </View>
    );
};

CursForm.defaultProps = {
    initialValues: {
        Denumire: '',
        Sala: ''
    }
};

const styles = StyleSheet.create({
    inputStyle: {
        fontSize: 18,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 15,
        padding: 5,
        margin: 5
    },
    labelStyle: {
        fontSize: 20,
        marginBottom: 5,
        marginLeft: 5
    }
});

export default CursForm;
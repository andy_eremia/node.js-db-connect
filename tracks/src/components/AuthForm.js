import React, {useState} from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Button, Input } from 'react-native-elements';
import Spacer from './Spacer';


const AuthForm = ({headerText, errorMessage, onSubmit, submitButtonText, navigation}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [nume, setName] = useState('');

    return (
        <>
            <Spacer>
                <Text h3>{headerText}</Text>
            </Spacer>
            {submitButtonText === "Sign up"
            ?    
                <Input
                label="Name"
                value={nume}
                onChangeText={setName} 
                autoCapitalize="none"
                autoCorrect={false}
                /> : null
            }
            <Spacer />
            <Input
                label="Email"
                value={email}
                onChangeText={setEmail} 
                autoCapitalize="none"
                autoCorrect={false}
            />
            <Spacer />
            <Input
                label="Password"
                value={password}
                onChangeText={setPassword} 
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry
            />
            {errorMessage ? <Text style={styles.errorMessageStyle}>{errorMessage}</Text> : null}
            <Spacer>
                {submitButtonText === "Sign up" ?
                    <Button
                        title={submitButtonText} 
                        onPress={() => onSubmit({nume, email, password}, () => navigation.navigate('Index'))}
                    /> :
                    <Button
                        title={submitButtonText} 
                        onPress={() => onSubmit({email, password}, () => navigation.navigate('Index'))}
                    />
                }
            </Spacer>  
        </>
    );
};

const styles = StyleSheet.create({
    errorMessageStyle: {
        fontSize: 16,
        color: 'red', 
        marginLeft: 15,
        marginTop: 15
      },
});

export default AuthForm;
import usersApi from '../api/users';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';


export const signup = dispatch => async ({ nume, email, password }, callback) => {
    try {
        const response = await usersApi.post('/register', { nume, email, password });
        console.log("Response data" + response.data);
        console.log("TOKEN: " + response.data.access_token);
        await AsyncStorage.setItem('token', response.data.access_token);
        console.log("ASYNC STORAGE" + await AsyncStorage.getItem('token'));
        dispatch({ type: 'signin', payload: response.data.access_token });
        if (callback) {
            callback();
        }
    } catch (err) {
        dispatch({ type: 'add_error', payload: 'Something went wrong while signing you up...' });
    }
};

export const tryLocalSignin = dispatch => async () => {
    const token = await AsyncStorage.getItem('token');
    console.log("teeeeeeest");
    console.log(token);
    const navigation = useNavigation();
    if (token) {
        dispatch({ type: 'signin', payload: token });
        navigation.navigate('Index');
    } else {
        navigation.navigate('Signup');
    }
};

export const clearErrorMessage = dispatch => () => {
    dispatch({ type: 'clear_error_message' });
};

export const signin = (dispatch) => async ({ email, password }, callback) => {
    try {
        const response = await usersApi.post('/login', { email, password });
        // console.log("Response data" + response.data);
        // console.log("TOKEN: " + response.data.access_token);
        await AsyncStorage.setItem('token', response.data.access_token);
        //console.log("ASYNC STORAGE" + await AsyncStorage.getItem('token'));

        dispatch({ type: 'signin', payload: response.data.access_token });
        if (callback) {
            callback();
        }
    } catch(err) {
        dispatch({ type: 'add_error', payload: 'Something went wrong while signing you in...' });
    }
};

export const signout = () => async (dispatch) => {
    try {
        await AsyncStorage.removeItem('token');
        dispatch({ type: 'signout'});
    } catch (err) {
        console.log(err.message);
    }
};



export const getCourses = () => async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
        // console.log("da");
    try {
        // console.log("fetch cursuri");
        // console.log(token);
        const response = await usersApi.get('/cursuri', {headers: {Authorization: 'Bearer ' + token}});
        //console.log(response.data);
        dispatch({ type: 'get_cursuri', payload: response.data });
    } catch (err) {
        dispatch({ type: 'get_error', payload: 'Something went wrong while fetching courses...' });
    }}
};

export const deleteCourse = (id) => async (dispatch) => {
    console.log(id);
    const token = await AsyncStorage.getItem('token');
    if (token) {
        try {
            await usersApi.delete(`/cursuri/${id}`, { headers: { Authorization: 'Bearer ' + token } });
            dispatch({ type: 'delete_curs', payload: id });
        } catch (err) {
            console.log(err.message);
        }
    }
};

export const editCurs = ({ idcursuri, Denumire, Sala }, callback) => async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
        try {
            await usersApi.put(`/cursuri/${idcursuri}`, { Denumire, Sala }, { headers: { Authorization: 'Bearer ' + token } });
            dispatch({ type: 'edit_curs', payload: { idcursuri, Denumire, Sala } });
            if (callback) {
                callback();
            }
        } catch (err) {
            console.log(err.message);
        }
    }
};

export const adaugaCurs = ({ Denumire, Sala }, callback) => async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
        try {
            const response = await usersApi.post('/cursuri', { Denumire, Sala }, { headers: { Authorization: 'Bearer ' + token } });
            console.log(response.data);
            dispatch({ type: 'add_curs', payload: { Denumire, Sala } });
            if (callback) {
                callback();
            }
        } catch (err) {
            console.log(err.message);
        }
    }
};

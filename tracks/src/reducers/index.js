import { combineReducers } from 'redux';
import authReducer from './authReducer';
import cursuriReducer from './cursuriReducer';

export default combineReducers({
    auth: authReducer,
    cursuri: cursuriReducer
});
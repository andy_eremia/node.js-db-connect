const initialState = {
    errorMessage: ''
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'add_curs':
            return {
                ...state,
                cursuriList: state.cursuriList.push(action.payload)
            }
        case 'edit_curs':
            return {
                ...state,
                cursuriList: state.cursuriList.map((curs) => {
                    return curs.idcursuri === action.payload.idcursuri
                        ? action.payload
                        : curs;
                })
            };
        case 'delete_curs':
            return state.cursuriList.filter((curs) => curs.idcursuri !== action.payload);
        case 'get_cursuri':
            return { ...state, cursuriList: action.payload, errorMessage: '' };
        case 'get_error':
            return { ...state, errorMessage: action.payload };
        default:
            return state;
    };
};
const initialState = {
    token: null,
    errorMessage: ''
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'add_error':
            return { ...state, errorMessage: action.payload };
        case 'signin':
            return { errorMessage: '', token: action.payload };
        case 'signout':
            return { token: null, errorMessage: '' };
        case 'clear_error_message':
            return { ...state, errorMessage: '' };
        default:
            return state;
    }
};
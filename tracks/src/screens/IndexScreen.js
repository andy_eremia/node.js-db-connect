import React, {useEffect} from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import { useSelector, useDispatch } from 'react-redux';
import { signout,getCourses,deleteCourse } from '../actions/index';
import Spacer from '../components/Spacer';
import Feather from 'react-native-vector-icons/Feather';

const IndexScreen = ({navigation}) => {

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCourses()); 
  }, []);
  
  const cursuri = useSelector(state => state.cursuri.cursuriList);
  console.log(cursuri);

  return (
    <View>
      <Text style={{ fontSize: 48 }}>Lista cursurilor: </Text>
      <Spacer />
      
      <FlatList 
        data={cursuri}
        keyExtractor={(curs) => curs.Denumire}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity onPress={() => navigation.navigate('Show', {idcursuri: item.idcursuri})}>
              <View style={styles.rowStyle}>
                <Text style={styles.titleStyle}>{item.Denumire} - {item.Sala}</Text>
                <TouchableOpacity onPress={() => {
                  dispatch(deleteCourse(item.idcursuri));
                  dispatch(getCourses());
                }}>
                  {/* <Feather style={styles.iconStyle} name="trash" /> */}
                  <Text>Sterge</Text>
                </TouchableOpacity>
              </View>  
            </TouchableOpacity>
          );
        }}
      />
      <Spacer>
        <Button 
          title="Sign out"
          onPress={() => {
            dispatch(signout());
            navigation.navigate('Signin');
          }}
          />
      </Spacer>
      <Spacer>
        <Button 
          onPress={() => navigation.navigate('Create')}
          title="Adauga curs"
        />
      </Spacer>
    </View>
  );
};


const styles = StyleSheet.create({
  rowStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    borderTopWidth: 1,
    borderColor: 'grey',
    paddingHorizontal: 10
  },
  titleStyle: {
      fontSize: 18
  },
  iconStyle: {
    fontSize: 24
  }
});

export default IndexScreen;

import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import CursForm from '../components/CursForm';
import {editCurs } from '../actions/index';

const EditScreen = ({ route, navigation }) => {
    const idcursuri = route.params.idcursuri;
    const cursuri = useSelector(state => state.cursuri.cursuriList);
    const curs = cursuri.find((curs) => curs.idcursuri === idcursuri);
    const dispatch = useDispatch();

    return (
        <CursForm
            initialValues={{ Denumire: curs.Denumire, Sala: curs.Sala }}
            onSubmit={(Denumire, Sala) => {
                dispatch(editCurs({idcursuri, Denumire, Sala}, () => navigation.pop()));
            }}
        />
    );
};

const styles = StyleSheet.create({});

export default EditScreen;
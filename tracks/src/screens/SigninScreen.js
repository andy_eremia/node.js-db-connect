import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { signin, clearErrorMessage } from '../actions/index';
import { View, StyleSheet } from 'react-native';
import AuthForm from '../components/AuthForm';
import NavLink from '../components/NavLink';

const SigninScreen = ({ navigation }) => {
  
  const errorMessage = useSelector(state => state.auth.errorMessage);
  
  const dispatch = useDispatch();

  return (
    <View style={styles.containerStyle}>
      <AuthForm 
        headerText="Sign in to your account"
        errorMessage={errorMessage}
        onSubmit={dispatch(signin)}
        submitButtonText="Sign in"
        navigation={navigation}
      />
      <NavLink
        text="Don't have an account? Sign up instead!"
        routeName='Signup'
        onClear={dispatch(clearErrorMessage)}
      />
    </View>
  );
};

const styles = StyleSheet.create({containerStyle: {
  flex: 1,
  justifyContent: 'center',
  marginBottom: 90
}
});

export default SigninScreen;

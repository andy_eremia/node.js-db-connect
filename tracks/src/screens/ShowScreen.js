import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'react-native-elements';
import { getCourses} from '../actions/index';

const ShowScreen = ({ navigation, route }) => {
    const cursuri = useSelector(state => state.cursuri.cursuriList);
    const curs = cursuri.find((curs) => curs.idcursuri === route.params.idcursuri);
    return (
        <View>
            <Text>{curs.Denumire}</Text>
            <Text>{curs.Sala}</Text>
            <Button 
                onPress={() => navigation.navigate('Edit', {idcursuri: route.params.idcursuri})}
                title="Editeaza curs"
            />
        </View>
    );
};

const styles = StyleSheet.create({});

export default ShowScreen;
import React, {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { signup, clearErrorMessage, tryLocalSignin } from '../actions/index';
import { View, StyleSheet } from 'react-native';
import AuthForm from '../components/AuthForm'; 
import NavLink from '../components/NavLink';
import AsyncStorage from '@react-native-community/async-storage';

const SignupScreen = ({ navigation }) => {
  
  const errorMessage = useSelector(state => state.auth.errorMessage);
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(tryLocalSignin);
  }, []);
  
  return (
    <View style={styles.containerStyle}>
      <AuthForm
        headerText="Sign up for Fullstack"
        errorMessage={errorMessage}
        submitButtonText="Sign up"
        onSubmit={dispatch(signup)}
        navigation={navigation}
      />
      <NavLink 
        routeName="Signin"
        text="Already have an account? Sign in instead!"
        onClear={dispatch(clearErrorMessage)}
      />
    </View>
  );
};


const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 90
  }
});

export default SignupScreen;

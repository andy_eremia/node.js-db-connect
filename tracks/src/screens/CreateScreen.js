import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';
import CursForm from '../components/CursForm';
import {adaugaCurs, getCourses } from '../actions/index';


const CreateScreen = ({navigation}) => {

    const dispatch = useDispatch();

    return (
        <CursForm 
            onSubmit={(Denumire, Sala) => {
                dispatch(adaugaCurs({ Denumire, Sala }, () => navigation.navigate('Index')));
                dispatch(getCourses());
            }}
        />
    );
};

const styles = StyleSheet.create({});

export default CreateScreen;